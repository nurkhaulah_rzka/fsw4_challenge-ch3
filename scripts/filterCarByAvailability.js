// This code was made by: @nurkhaulah_rzka

function filterCarByAvailability(cars) {
  // Sangat dianjurkan untuk console.log semua hal
  console.log(cars);

  // Tempat penampungan hasil
  const result = [];

  // My code for filter car by availability
  for (let a = 0; a < cars.length; a++) {
    if (cars[a].available) {
      result.push(cars[a]);
    }
  }

  // Array hasil filter berdasarkan availablity
  console.log(result);
  return result;
  
}