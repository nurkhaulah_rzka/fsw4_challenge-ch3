// This code was made by: @nurkhaulah_rzka

function sortCarByYearDescendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal
  console.log(cars);

  // Clone array untuk menghindari side-effect
  const result = [...cars];

  // My code for sort car by year descendingly
  for (let a = 0; a < result.length; a++) {
    for (let b = 0; b < result.length - 1; b++) {
      if (result[b].year < result[b + 1].year) {
        const temp = result[b];
        result[b] = result[b + 1];
        result[b + 1] = temp;
      }
    }
  }

  // Array hasil sorting secara descending
  console.log(result);
  return result;
}